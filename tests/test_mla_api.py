import urllib.request, json
import sys
sys.path.append('../ml_sdk/lib')
from ml_sdk.lib.meli import Meli


class TestMlaApi:
    def __init__(self):
        publicaciones = 0
        exit = False
        offset = 0
        while not exit:
            self.api = f'https://api.mercadolibre.com/sites/MLA/search?search_type=scan&q=s10&offset={offset}'
            offset += 50
            try:
                response = urllib.request.urlopen(self.api)
            except:
                break
            data = json.loads(response.read())
            publicaciones += len(data['results'])
            for pub in data['results']:
                print("<---------------------------------------->")
                print(pub['title'])
                print(pub['price'])
            print(json.dumps(data['results']))
        print("Cantidad de publicaciones:")
        print(publicaciones)

    def test_meli(self):
        meli_connection = Meli(client_id='7662247107102941', client_secret='nNA1eX6SjLPyxYCwgpkZc00m2ZwQI9uZ',
                               refresh_token=True)
        meli_connection.authorize(code=200, redirect_URI='localhost')
        print(meli_connection.access_token)


mla = TestMlaApi()
