from rest_framework import viewsets
from django.shortcuts import render
from rest_framework import generics
from offers.models import Offer
from offers.serializers import OfferSerializer
from providers.views import MLAProvider
from itertools import chain
from rest_framework.exceptions import ParseError
import logging

logger = logging.getLogger(__name__)


class OfferViewSet(generics.ListAPIView):
    """
    Provides a get method handler.
    """
    serializer_class = OfferSerializer

    def get_queryset(self):
        search_text = self.request.query_params.get('search')
        if not search_text:
            raise ParseError
        mla_provider = MLAProvider()
        empty_query_set = Offer.objects.none()
        query_set = list(chain(empty_query_set, mla_provider.get_offers(search_text=search_text)))
        return query_set
