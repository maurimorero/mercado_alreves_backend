from rest_framework import serializers
from offers.models import Offer


class OfferSerializer(serializers.HyperlinkedModelSerializer):
    provider = serializers.StringRelatedField(many=False)

    class Meta:
        model = Offer
        fields = ['product_model', 'product_brand', 'price', 'product_thumbnail', 'offer_link', 'provider', 'condition',
                  'free_shipping', 'score']
