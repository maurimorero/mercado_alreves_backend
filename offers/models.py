from __future__ import unicode_literals
from providers.models import Provider
from django.db import models


class Offer(models.Model):
    product_model = models.CharField(max_length=400)
    product_brand = models.CharField(max_length=400)
    product_thumbnail = models.URLField(max_length=450,
                                        default='https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/'
                                                'iphone8-silver-select-2018?wid=940&hei=1112&fmt=png-alpha&qlt=80&.'
                                                'v=1550795416961')
    offer_link = models.URLField(max_length=450, default='https://www.mercadolibre.com.ar/')
    price = models.FloatField(null=True, default=0.00)
    condition = models.CharField(max_length=50, default='New')
    free_shipping = models.BooleanField(null=True, default=False)
    days_to_deliver = models.IntegerField(null=True, default=None)
    score = models.CharField(max_length=50, null=True, default=None)
    provider = models.ForeignKey(Provider, default=None, on_delete=models.CASCADE)

    def __str__(self):
        return self.product_model
