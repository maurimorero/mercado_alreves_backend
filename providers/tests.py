from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
from providers.models import Provider
from providers.serializers import ProviderSerializer

# tests for views


class BaseViewTest(APITestCase):
    client = APIClient()

    @staticmethod
    def create_provider(name="", description=""):
        if name != "" and description != "":
            Provider.objects.create(name=name, description=description)

    def setUp(self):
        # add test data
        self.create_provider("MLA", "Mercado Libre Argentina")
        self.create_provider("MLP", "Mercado Libre Peru")
        self.create_provider("MLB", "Mercado Libre Brasil")
        self.create_provider("MLU", "Mercado Libre Uruguay")


class GetAllProviderTest(BaseViewTest):

    def test_get_all_providers(self):
        """
        This test ensures that all providers added in the setUp method
        exist when we make a GET request to the providers/ endpoint
        """
        # hit the API endpoint
        response = self.client.get(
            reverse("providers-all", kwargs={"version": "v1"})
        )
        # fetch the data from db
        expected = Provider.objects.all()
        serialized = ProviderSerializer(expected, many=True)
        self.assertEqual(response.data, serialized.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)