from __future__ import unicode_literals
from django.db import models
from jsonfield import JSONField


class Provider(models.Model):
    name = models.CharField(unique=True, max_length=400)
    description = models.TextField(blank=True, null=True, default=None)

    def __str__(self):
        return self.name


class API(models.Model):
    description = models.CharField(max_length=400, default=None)
    api = JSONField(default=None)
    provider = models.ForeignKey(Provider, default=None, on_delete=models.CASCADE)

    def __str__(self):
        return self.description

