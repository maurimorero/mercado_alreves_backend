from django.contrib import admin
from providers.models import Provider, API

admin.site.register(Provider)
admin.site.register(API)
