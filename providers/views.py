from rest_framework import viewsets
from django.shortcuts import render
from rest_framework import generics
from providers.models import Provider, API
from providers.serializers import ProviderSerializer
import urllib.request, json
from offers.models import Offer
import logging

logger = logging.getLogger(__name__)


class ProviderViewSet(generics.ListAPIView):
    """
    Provides a get method handler.
    """
    queryset = Provider.objects.all()
    serializer_class = ProviderSerializer


class MLAProvider(Provider):
    def __init__(self):
        self.provider = Provider.objects.get(name='MLA')
        self.api = API.objects.filter(provider=self.provider)[0].api
        self.api_built = None

    def build_mla_api_with_defaults(self, search_text):
        url = self.api['prefix']
        for idx, item in enumerate(self.api['attributes']):
            if item['name'] == 'q':
                if idx != 0:
                    url += '&'
                url += f"{item['name']}={search_text}"
            else:
                if idx != 0:
                    url += '&'
                url += f"{item['name']}={item['default_value']}"
        self.api_built = url
        return self.api_built

    def _build_mla_api_with_offset(self, search_text, offset):
        url = self.api['prefix']
        for idx, item in enumerate(self.api['attributes']):
            if idx != 0:
                url += '&'
            if item['name'] == 'q':
                url += f"{item['name']}={search_text}"
            elif item['name'] == 'offset':
                url += f"{item['name']}={offset}"
            else:
                url += f"{item['name']}={item['default_value']}"
        self.api_built = url
        return self.api_built

    def get_offers(self, search_text):
        # Replace spaces with underscore
        search_text.replace(' ', '_')
        exit = False
        offset = 0
        offers = []
        while not exit:
            self._build_mla_api_with_offset(search_text=search_text, offset=offset)
            offset += 50
            try:
                logger.warn(f"Getting offers from MLA provider - Query: {self.api_built} ...")
                response = urllib.request.urlopen(self.api_built)
            except:
                logger.error(f"Error getting offers from MLA provider - Query: {self.api_built}")
                break
            data = json.loads(response.read())
            if len(data['results']) == 0:
                # TODO Logger
                break
            for pub in data['results']:
                offer = Offer()
                offer.offer_link = pub['permalink']
                offer.product_thumbnail = pub['thumbnail']
                offer.price = pub['price']
                offer.product_brand = 'Unknown'
                offer.product_model = 'Unknown'
                offer.provider = self.provider
                offer.free_shipping = pub.get('shipping', {}).get('free_shipping')
                offer.score = pub.get('seller', {}).get('power_seller_status')
                for attribute in pub['attributes']:
                    if attribute['id'] == 'BRAND':
                        offer.product_brand = attribute['value_name']
                    elif attribute['id'] == 'MODEL':
                        offer.product_model = attribute['value_name']
                    elif attribute['id'] == 'ITEM_CONDITION':
                        offer.condition = attribute['value_name']
                offers.append(offer)
        return offers
        # If want the offers sorted by price
        # sorted(offers, key=lambda x: x.price, reverse=False)

